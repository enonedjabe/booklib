import { Switch, Route, Link, BrowserRouter as Router } from "react-router-dom";
import BookList from "./components/BookList";
import BookDetails from "./components/BookDetails";
import "./styles.css";
import { ToasterProvider } from "./ui/ToasterContext";
import React ,{useState,useEffect } from "react";
import firebase from "./setupFirebase";
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth'
import 'bootstrap/dist/css/bootstrap.min.css';



function App() {
  const [isSignedIn, setSignedIn] = useState(false);
  const uiConfig = {
    signInFlow:"popup",
    signInOptions: [
      //firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      firebase.auth.EmailAuthProvider.PROVIDER_ID,
    ],
    callbacks:{
      signInSuccess:() => false,
    },
  };

  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) =>{
      setSignedIn(!!user)
      console.log(user)
    });
  }, [])
  return (
   <>
   {isSignedIn ?(
         <Router>
         <ToasterProvider>
           <div className="App">
           <nav className="navbar navbar-expand-lg navbar-light bg-light">
             <div class="container-fluid">

                <a className="navbar-brand mt-2 mt-lg-0" href="#">
                  <Link to="/">Books library</Link>
                </a>
                <div className="d-flex align-items-center">
                  <a
                  className="text-reset me-3  hidden-arrow"
                  href="#"
                  id="navbarDropdownMenuLink"
                  role="button"
                  >
                    <span className="btn btn-primary" onClick={() => firebase.auth().signOut()}>se deconnecter</span>
                    </a>
                    <a
                      className=" d-flex align-items-center hidden-arrow"
                      href="#"
                      id="navbarDropdownMenuLink"
                      role="button"
                      data-mdb-toggle="dropdown"
                      aria-expanded="false"
                    >
                      <img
                        src={firebase.auth().currentUser.photoURL}
                        class="rounded-circle"
                        height="25"
                        alt=""
                        loading="lazy"
                      />
                      <strong className="d-none d-sm-block ms-1">{firebase.auth().currentUser.displayName}</strong>
                    </a>
    
    </div>

  </div>

</nav>

             {/*<h1>
               <Link to="/">Books library</Link>
             </h1>*/}
   
             <Switch>
               <Route path="/" exact>
                 <BookList />
               </Route>
               <Route path="/book/:id">
                 <BookDetails />
               </Route>
             </Switch>
           </div>
         </ToasterProvider>
       </Router>

   ):(
     <div className="login-page text-center " style={{margin:'15% auto'}}>
       <h1 className=''>Connexion</h1>
       <StyledFirebaseAuth
       uiConfig ={uiConfig}
       firebaseAuth={firebase.auth()}

       />

     </div>

   )}
   </>
  );
}

export default App;