import { useState, useEffect } from "react";
import firebase from "firebase/app";
import BookItem from './BookItem';
import {Loading} from '../ui';
import AddBook from "./AddBook";

function BookList() {
  const [books, setBooks] = useState([]);

  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  useEffect(() => {
    const db = firebase.firestore();

    (async () => {
      const snapshot = await db.collection("books").get();
      const booksArray = [];
      snapshot.forEach((doc) => {
        booksArray.push({
          id: doc.id,
          ...doc.data(),
        });
      });
      setBooks(booksArray);
    })();
  }, []);

  return (
    <>
   
   
       <AddBook modal={modal} setModal={setModal} toggle={toggle} />
       <section className="vh-100" >
        <div className="container h-100">
          <div className="row d-flex  h-100">
            <div className="col-xl-9">
              <h1 className=" mb-4">Book list</h1>
                <div className="" >
                  <div className="card-body">
                  <div className="row align-items-center pt-4 pb-3">

                  {!books.length ? <Loading /> : books.map((book) => (
                    <BookItem book={book} key={book.id} />
                    ))}

                  </div>
                    
          </div>
        </div>

      </div>
    </div>
  </div>
</section>  

      
      
      
    </>
  );
}


export default BookList;