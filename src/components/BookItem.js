import {Link} from 'react-router-dom'

function BookItem({ book }) {
  return (
    <>
      <div className="col-md-3 ">
        <h4 className="mb-0"><Link to={`/book/${book.id}`}>{book.title}</Link></h4>
      </div>
      <div className="col-md-3 pe-5">

      <span>
        <strong>Pages: </strong> {book.pages}
      </span>

      </div>
      <div className="col-md-6 pe-5">
          <span>
            <strong>Publishing Date: </strong>{" "}
            {book.publishDate.toDate().toDateString()}
            </span>
      </div>
      
      
    </>
  );
}




export default BookItem;