import { Link, useLocation } from "react-router-dom";
import styled from "styled-components";


const StyledUl = styled.ul`
  display: flex;
  flex-direction:row;
  justify-content: center;
  align-items: center;
  list-style: none;
  padding: 0;
  li {
    font-size: 20px;
    text-align: center;
    padding: 6px;
    margin: 4px;
    background-color: #f0f0f0;
    display: inline-block;
    &.active {
        background-color: #ccc;
    }
    @media (max-width: 28.75em) {
      li{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    
      }
    }
   
  }

`;

function BookMenu({ url }) {
  const location = useLocation();
  return (
    < >
      <StyledUl>
      <li className={(location.pathname === url && 'active').toString()}>
        <Link to={`${url}`}>General information</Link>
      </li>
      <li className={(location.pathname === `${url}/authors` && 'active').toString()}>
        <Link to={`${url}/authors`}>Authors</Link>
      </li>
      <li className={(location.pathname === `${url}/photos` && 'active').toString()}>
        <Link to={`${url}/photos`}>Photos</Link>
      </li>
    </StyledUl>

    </>
    
  );
}


export default BookMenu;