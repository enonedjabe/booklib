import EditBook from '../EditBook';

function BookGeneral({ book, id ,modal,toggle,setModal}) {
  return (
    <>
    <section className="vh-100" >
        <div className="container h-100">
          <div className="row d-flex  h-100">
            <div className="col-xl-9">
            <h1>General</h1>
            <div className="" >
             <div className="card-body">
            <EditBook id={id} book={book} modal={modal} toggle={toggle} setModal={setModal} />
            </div>
            </div>
            </div>
            </div>
            </div>
            </section>
      
      
    </>
  );
}

export default BookGeneral;
