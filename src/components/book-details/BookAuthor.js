import {useContext} from 'react';
import firebase from 'firebase/app';
import {ToasterContext} from '../../ui/ToasterContext';
import imgProfile from '../../profile-placeholder.png';
import {excerpt} from '../../functions/stringFn';
import 'bootstrap/dist/css/bootstrap.min.css';

function Author({author, id, dispatch}) {
    const {addToast} = useContext(ToasterContext);
    const handleDelete = async (e) => {
        e.preventDefault();
        try {
            if(author.photo !== '') await firebase.storage().refFromURL(author.photo).delete();
            await firebase.firestore().collection('books').doc(id).update({
                authors: firebase.firestore.FieldValue.arrayRemove(author)
            });
            dispatch({type: 'removeAuthor', name: author.name});
            addToast({text: "Remove author successfully", type: 'success'});
        } catch (e) {
            console.log(e);
            addToast({text: 'Error deleting author', type: 'error'});
        }
    }
    return (
        <>
                <div className="container mt-4">
                    <div className="row">
                        <div className="col-md-4">
                            <img className="img-fluid" src={author.photo !== '' ? author.photo : imgProfile } style={{width:'18rem'}}/>    
                        </div>
                        <div className="col-md-8">
                            <div className="card-body">
                                <h5 className="card-title">{author.name} </h5>
                                <p className="card-text">{author.description}</p>
                            </div>
                            <button type="button" className="btn btn-danger m-2" onClick={handleDelete}> Delete </button>
                            
                        </div>
                    </div>
                </div>

   

   

                
        

        </>
    )
}

export default Author