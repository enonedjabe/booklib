import styled from 'styled-components';

const StyledDiv = styled.div`
    margin-bottom: 15px;
    display: flex;
    border-bottom: #f0f0f0 1px solid;
    padding-bottom: 20px;
`;

const StyledLabel = styled.label`
    font-weight: bold;
    width: 120px;
`;

function Field(props) {
    return (
        <>
            <div className="row align-items-center pt-4 pb-3">
                <div className="col-md-3 ps-5">
                    <h6 className="mb-0"  htmlFor={props.id}>{props.labelText}</h6>
                </div>
                
                <div className="col-md-9 pe-5">

                    {props.children}

                </div>
            </div>
            <hr className="mx-n3"/>
        </>
    )
}

export default Field;