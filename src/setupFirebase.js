import firebase from "firebase";
import  "firebase/analytics";
import "firebase/firestore";
import "firebase/storage";

const firebaseConfig = {
    apiKey: "AIzaSyBvOaX6lVGLvn9XE_ysUMXSthTohPFYlic",
    authDomain: "bookslib-4c1f7.firebaseapp.com",
    projectId: "bookslib-4c1f7",
    storageBucket: "bookslib-4c1f7.appspot.com",
    messagingSenderId: "907590778748",
    appId: "1:907590778748:web:6d54084c150a6d9303eef9",
    measurementId: "G-58CTHDZBLF"
  };

  firebase.initializeApp(firebaseConfig)

  export default firebase